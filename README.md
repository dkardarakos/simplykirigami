# Simply Kirigami

Simply Kirigami is a Kirigami application, based on the Kirigami application template. Its purpose is to facilitate easy building of Kirigami applications on Linux and Android.

## Linux

On Linux, it is recommended to build with cmake and link Kirigami at runtime as a dynamic plugin. 

Before getting started, ensure that Qt and Kirigami have been installed, using your distribution packages.

### Build

```
cmake -DKDE_INSTALL_USE_QT_SYS_PATHS=ON  ..
make 
```

### Install

```
sudo make install
```

### Run

```
simplykirigami
```

![Desktop](./screenshots/desktop.png)

If you would like a Plasma Mobile look and feel:

```
QT_QUICK_CONTROLS_MOBILE=true QT_QUICK_CONTROLS_STYLE=Plasma simplykirigami
```

![Mobile](./screenshots/mobile.png)

## Android

To build for Android, we suggest using the [KDE docker image](https://community.kde.org/Android/Environment_via_Container) that makes the installation of dependencies easy. It also sets all the environment variables we are going to need. So, before getting started with the Android build, docker should be installed.

### Run docker

Start the *KDE for Android* docker image:

```
docker run -ti --rm --mount type=bind,source=/tmp,target=/output --mount type=bind,source="$(pwd)",target=/home/user/src kdeorg/android-arm-sdk bash
```

The above mentioned command:

- Runs the docker image (it will be downloaded the first time you use it)
- Offers a shell
- Bind-mounts /output of the container to /tmp of the host
- Bind-mounts /home/user/src/ of the container to the current directory
- Automatically cleans up the container and removes the file system when the container exits

### Build dependencies

Our project depends on Ki18n, to support multiple languages. It also needs Kirigami. To build these packages, execute the following on the docker image:

```
export PATH=/opt/helpers/:$PATH
build-kde-dependencies kirigami
build-kde-project kirigami kirigami
build-kde-dependencies ki18n
build-kde-project ki18n ki18n
```

### Build the application

Now, we are going to build the application and create the Android apk. 

On the docker image:

```
build-cmake simplykirigami Applications "-DQTANDROID_EXPORTED_TARGET=simplykirigami -DANDROID_APK_DIR=/home/user/src/simplykirigami/android"
create-apk simplykirigami
cp /home/user/build/simplykirigami/simplykirigami_build_apk/build/outputs/apk/debug/simplykirigami_build_apk-debug.apk /output/
```

### Install 

Since /output of the docker image is bind-mount on /tmp of the host, the apk is available in our host machine's /tmp directory.

Ensure that the phone is connected to your host and debugging is enabled.

Install on the phone:

```
adb install -r /tmp/simplykirigami_build_apk-debug.apk
```

![Android](./screenshots/android.png)
